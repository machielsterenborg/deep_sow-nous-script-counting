import os
import random
import re
from typing import Dict

import cv2
import keras.backend as backend
import numpy as np
import pandas
from PIL import Image, ImageFont, ImageDraw
from skimage.io import imread


def draw_boxes_and_count_on_image(image_filename: str, processed_prediction: [], found_boxes: int) -> np.array:
    """
    :param image_filename: filename of image to draw on
    :param processed_prediction: List containing one or more pairs of bboxes and labels
    :param found_boxes: Number of valid boxes found for this frame
    :return: A np array containing our manipulated image
    If the BBOX is empty, the were no bounding boxes which passed the confidence check
    """

    # Prepare the image to be edited
    frame = imread(image_filename)
    frame = Image.fromarray(np.uint8(frame)).convert("RGB")

    # Draw all the given bboxes and labels onto a copy of the image and return it

    for bbox, label_idx, label in processed_prediction:
        color = generate_unique_color_for_bbox(int(label_idx))
        frame = draw_bounding_box_on_image(frame, bbox[0], bbox[1], bbox[2], bbox[3], color=color,
                                           label=[label])

    try:
        font = ImageFont.truetype('arial.ttf', 32)
    except IOError:
        print("font could not be loaded, loading default font")
        font = ImageFont.load_default()

    frame = draw_count_on_frame(frame, found_boxes, font)

    return frame


def draw_count_on_frame(frame: np.array, found_boxes: int, font) -> np.array:
    draw = ImageDraw.Draw(frame)
    draw.text((10, 10), str(found_boxes), font=font, fill='black')

    return frame


def draw_bounding_box_on_image(image, xmin, ymin, xmax, ymax, color, thk=2, label=(), use_norm_coord=True):
    """
    Adds a bounding box to an image.
      Bounding box coordinates can be specified in either absolute (pixel) or
      normalized coordinates by setting the use_normalized_coordinates argument.
      Each string in display_str_list is displayed on a separate line above the
      bounding box in black text on a rectangle filled with the input 'color'.
      If the top of the bounding box extends to the edge of the image, the strings
      are displayed below the bounding box.
      Args:
        image: a PIL.Image object.
        ymin: ymin of bounding box.
        xmin: xmin of bounding box.
        ymax: ymax of bounding box.
        xmax: xmax of bounding box.
        color: color to draw bounding box. Default is green.
        thk: line thickness. Default value is 4.
        label: list of strings to display in box
                          (each to be shown on its own line).
        use_norm_coord: If True (default), treat coordinates
          ymin, xmin, ymax, xmax as relative to the image.  Otherwise treat
          coordinates as absolute.
      """
    draw = ImageDraw.Draw(image)
    im_width, im_height = image.size
    if use_norm_coord:
        (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                      ymin * im_height, ymax * im_height)
    else:
        (left, right, top, bottom) = (xmin, xmax, ymin, ymax)
    draw.line([(left, top), (left, bottom), (right, bottom),
               (right, top), (left, top)], width=thk, fill=color)
    try:
        font = ImageFont.truetype('arial.ttf', 20)
        print("font could not be loaded, loading default font")
    except IOError:
        font = ImageFont.load_default()

    # If the total height of the display strings added to the top of the bounding
    # box exceeds the top of the image, stack the strings below the bounding box
    # instead of above.
    display_str_heights = [font.getsize(ds)[1] for ds in label]
    # Each display_str has a top and bottom margin of 0.05x.
    total_display_str_height = (1 + 2 * 0.05) * sum(display_str_heights)

    if top > total_display_str_height:
        text_bottom = top
    else:
        text_bottom = bottom + total_display_str_height
    # Reverse list and print from bottom to top.
    for display_str in label[::-1]:
        text_width, text_height = font.getsize(display_str)
        margin = np.ceil(0.05 * text_height)

        draw.rectangle([(left, text_bottom - text_height - 2 * margin), (left + text_width, text_bottom)], fill=color)
        draw.text((left + margin, text_bottom - text_height - margin), display_str, fill='black', font=font)

        text_bottom -= text_height - 2 * margin

    return image


def load_data_for_detection_inference(filenames, pad_size, image_min_side, image_max_side):
    """
    Loads data required for inference on detection task
    :param filenames: image filenames (including extension)
    :param pad_size: number of pixels used for padding
    :param image_min_side: see keras-retinanet image_min_side
    :param image_max_side: see keras-retinanet image_max_side
    :return: uids, images, scales, heights, widths, size_before_padding, target_side
    """
    images = []
    images_before_padding = []
    uids = []
    scales = []
    size_before_padding = []
    heights = []
    widths = []
    for filename in filenames:
        print("current file being loaded: " + filename)
        uid = os.path.splitext(filename)[0]
        uids.append(uid)

        image = read_image_bgr(filename)
        original_shape = image.shape
        height, width = original_shape[:2]
        heights.append(height)
        widths.append(width)

        # - pad the border of the image, to make object detection at the border better
        new_im = np.zeros(
            shape=(image.shape[0] + (pad_size * 2), image.shape[1] + (pad_size * 2), image.shape[2]))
        new_im[pad_size:pad_size + image.shape[0], pad_size:pad_size + image.shape[1], :] = image
        image = new_im

        # preprocess image for network
        image = preprocess_image(image)

        image, scale = resize_image(image, min_side=image_min_side, max_side=image_max_side)
        scales.append(scale)
        size_before_padding.append(image.shape[0:2])
        images_before_padding.append(image)

    # make the images to be of the same size, by squaring
    max_shape = tuple(max(image.shape[x] for image in images_before_padding) for x in range(3))
    target_side = max(max_shape)
    for image in images_before_padding:
        image = nous_resize_image(image, target_size=[target_side, target_side], squaring_method="pad")
        images.append(np.expand_dims(image, axis=0))

    return uids, images, scales, heights, widths, size_before_padding, target_side


def preprocess_image(x: np.array, mode='tf') -> np.array:
    """ Preprocess an image by subtracting the ImageNet mean.

    Args
        x: np.array of shape (None, None, 3) or (3, None, None).
        mode: One of "caffe" or "tf".
            - caffe: will zero-center each color channel with
                respect to the ImageNet dataset, without scaling.
            - tf: will scale pixels between -1 and 1, sample-wise.

    Returns
        The input with the ImageNet mean subtracted.
    """
    # mostly identical to "https://github.com/keras-team/keras-applications/blob/master/keras_applications/imagenet_utils.py"
    # except for converting RGB -> BGR since we assume BGR already
    x = x.astype(backend.floatx())
    if mode == 'tf':
        x /= 127.5
        x -= 1.
    elif mode == 'caffe':
        x[..., 0] -= 103.939
        x[..., 1] -= 116.779
        x[..., 2] -= 123.68

    return x


def resize_image(img, min_side=800, max_side=1333) -> tuple:
    """ Resize an image such that the size is constrained to min_side and max_side.

    Args
        min_side: The image's min side will be equal to min_side after resizing.
        max_side: If after resizing the image's max side is above max_side, resize until the max side is equal to max_side.

    Returns
        A resized image.
    """
    (rows, cols, _) = img.shape

    smallest_side = min(rows, cols)

    # rescale the image so the smallest side is min_side
    scale = min_side / smallest_side

    # check if the largest side is now greater than max_side, which can happen
    # when images have a large aspect ratio
    largest_side = max(rows, cols)
    if largest_side * scale > max_side:
        scale = max_side / largest_side

    # resize the image with the computed scale
    img = cv2.resize(img, None, fx=scale, fy=scale)

    return img, scale


def compensate_squaring_on_bounding_boxes(predicted_boxes, original_size, current_size) -> []:
    """
    Transforms the box from squared image coordinate to the original image coordinate
    :param predicted_boxes: ndarray Nx4, N number of boxes, (x1, y1, x2, y2)
    :param original_size: original image size before squaring
    :param current_size: image size after squaring
    :return:
    """
    square_scale = float(max(original_size)) / float(current_size)
    predicted_boxes *= square_scale
    diff_height = (current_size * square_scale) - original_size[0]
    diff_width = (current_size * square_scale) - original_size[1]
    predicted_boxes[:, 0] -= (diff_width / 2)
    predicted_boxes[:, 1] -= (diff_height / 2)
    predicted_boxes[:, 2] -= (diff_width / 2)
    predicted_boxes[:, 3] -= (diff_height / 2)

    return predicted_boxes


def get_class_id_to_name(classes_path, true_labels=None) -> Dict[int, str]:
    """
    Get class mapping with class index as key and class name as value.
    :param classes_path: headerless txt file with columns "classname","index"
    :return: map from class id to class name
    """
    classes = pandas.read_csv(classes_path, header=None)
    if true_labels is not None:
        for i_class, class_name in enumerate(classes[0].values.tolist()):
            for true_label in true_labels:
                if true_label.lower() == str(class_name).lower():
                    classes.loc[i_class, 0] = true_label
    return dict(zip(classes[1].values, classes[0].values))


def create_buffer_table_from_detection_results(uid, predicted_boxes, predicted_labels, predicted_scores,
                                               confidence_threshold, pad_size, width, height, class_id_to_name) -> []:
    """
    Create JSON from detection predictions
    :param uid: image uid
    :param predicted_boxes: ndarray Nx4, N number of boxes, (x1, y1, x2, y2)
    :param predicted_labels: ndarray N
    :param predicted_scores: ndarray N
    :param confidence_threshold: minimum confidence to accept prediction
    :param pad_size: number of pixels used for padding
    :param width: original image width
    :param height: original image height
    :param class_id_to_name: map of class id to class name
    :return: table_boxes for active learning with headers ["uid", "label_idx", "label", "score", "x1", "y1", "x2", "y2"]
    """
    table_boxes = []
    for idx, (label, score) in enumerate(zip(predicted_labels, predicted_scores)):
        if score < confidence_threshold:
            continue
        b = predicted_boxes[idx].astype(int)
        x1 = (b[0] - pad_size) / float(width)
        y1 = (b[1] - pad_size) / float(height)
        x2 = (b[2] - pad_size) / float(width)
        y2 = (b[3] - pad_size) / float(height)

        table_boxes.append((uid, label, class_id_to_name[label], score, x1, y1, x2, y2))

    return table_boxes


def read_label_file() -> []:
    """
    :return: Text version of label taken from labels.txt which should be in the main folder
    """
    path = "labels.txt"
    if os.path.isfile(path):
        with open(path) as f:
            label_list = f.readlines()
    else:
        print("labels.txt could not be opened")
        quit()
    return label_list


def get_num_classes() -> int:
    '''
    :return: Length of list generated by read_label_file, which corresponds to the ammount of classes
    '''
    return len(read_label_file())


def generate_unique_color_for_bbox(label: int) -> tuple:
    """
    :label: Numerical label
    :return: Tuple contraining unique rgb value based on numerical input label
    """
    random.seed(label)
    r = random.randint(0, 225)
    g = random.randint(1, 224)
    b = random.randint(2, 223)

    return r, g, b


def read_image_bgr(path: str) -> np.array:
    """ Read an image in BGR format.

    Args
        path: Path to the image.
    """
    image = np.asarray(Image.open(path).convert('RGB'))

    return image[:, :, ::-1]  # .copy()


def natural_keys(text: str):
    """
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    """

    return [atoi(c) for c in re.split(r'(\d+)', text)]


def non_max_suppression(boxes, scores, labels, num_classes, nms_threshold=0.5, score_threshold=0.5):
    """
    Removes overlapping boxes using non-max-suppression.
    Adapted from: https://www.pyimagesearch.com/2015/02/16/faster-non-maximum-suppression-python/
    :param boxes: list of 4-element tuple representing the boxes (x1, y1, x2, y2)
    :param scores: list of confidence scores corresponding to the boxes
    :param labels: list of labels corresponding to the boxes (integer)
    :param num_classes: number of classes
    :param nms_threshold:
    :param score_threshold:
    :return: box indices
    """
    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return []

    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions
    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

    # initialize the list of picked indexes
    pick = []

    # grab the coordinates of the bounding boxes
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]

    # keep looping while some indexes still remain in the indexes
    # list
    # invalid_boxes_idx = np.union1d(np.union1d(np.union1d(np.union1d(np.where(x1 < 0), np.where(x2 < 0)),
    #                                           np.where(y1<0)),
    #                                np.where(y2<0)),
    #                                np.where(scores < score_threshold))
    x1[np.where(x1 < 0)] = 0
    y1[np.where(y1 < 0)] = 0

    invalid_boxes_idx = np.union1d(np.where(np.logical_or(x2 < 0, y2 < 0)), np.where(scores < score_threshold))
    x1 = np.delete(x1, invalid_boxes_idx)
    x2 = np.delete(x2, invalid_boxes_idx)
    y1 = np.delete(y1, invalid_boxes_idx)
    y2 = np.delete(y2, invalid_boxes_idx)
    labels = np.delete(labels, invalid_boxes_idx)

    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    indices = np.array(range(len(x1)))

    original_indices = indices.copy()
    for i_class in range(num_classes):
        indices_class = np.where(labels == i_class)
        indices = np.intersect1d(original_indices, indices_class)
        while len(indices) > 0:
            # grab the last index in the indexes list and add the
            # index value to the list of picked indexes
            last = len(indices) - 1
            i = indices[last]
            # pick.append(i)

            # find the largest (x, y) coordinates for the start of
            # the bounding box and the smallest (x, y) coordinates
            # for the end of the bounding box
            xx1 = np.maximum(x1[i], x1[indices[:last]])
            yy1 = np.maximum(y1[i], y1[indices[:last]])
            xx2 = np.minimum(x2[i], x2[indices[:last]])
            yy2 = np.minimum(y2[i], y2[indices[:last]])

            # compute the width and height of the bounding box
            w = np.maximum(0, xx2 - xx1 + 1)
            h = np.maximum(0, yy2 - yy1 + 1)

            # compute the ratio of overlap with other boxes
            overlap = (w * h) / area[indices[:last]]

            # compute the ratio of overlap with current box, for nested rectangles
            overlap_2 = (w * h) / area[indices[last]]

            # delete all indexes from the index list that have
            all_overlapping = np.unique(np.concatenate(([last], np.where(overlap > nms_threshold)[0],
                                                        np.where(overlap_2 > nms_threshold)[0])))

            # take a pick with max score
            overlapping_scores = scores[all_overlapping]
            max_idx = np.argmax(overlapping_scores)
            idx_to_pick = indices[all_overlapping[max_idx]]

            pick.append(idx_to_pick)
            indices = np.delete(indices, all_overlapping)

    # return only the indices of bounding boxes to pick
    return pick


def nous_resize_image(img, target_size, squaring_method=None):
    input_array = isinstance(img, np.ndarray)
    if input_array:  # - convert to pil
        img = array_to_img(img)

    hw_tuple = (target_size[1], target_size[0])
    if img.size != hw_tuple:
        if squaring_method is None:
            img = img.resize(hw_tuple)
        else:
            if squaring_method == "pad":
                # pad
                new_size = (max(img.size),) * 2
                new_im = Image.new("RGB", new_size)  # luckily, this is already black!
                new_im.paste(img, (int((new_size[0] - img.size[0]) / 2),
                                   int((new_size[1] - img.size[1]) / 2)))

                if hw_tuple[0] > -1:
                    img = new_im.resize(hw_tuple)
                else:
                    img = new_im
            # img.show()
            elif squaring_method == "crop":
                min_size = min(img.size)
                img = img.crop(
                    ((img.size[0] - min_size) / 2, (img.size[1] - min_size) / 2, (img.size[0] + min_size) / 2,
                     (img.size[1] + min_size) / 2))
                img = img.resize(hw_tuple)

    if input_array:  # - convert back to np
        img = img_to_array(img)

    return img


def array_to_img(x, data_format=None, scale=True):
    """CCopied from keras with some modification to remove keras dependency.
    Converts a 3D Numpy array to a PIL Image instance.

    # Arguments
        x: Input Numpy array.
        data_format: Image data format.
        scale: Whether to rescale image values
            to be within [0, 255].

    # Returns
        A PIL Image instance.

    # Raises
        ImportError: if PIL is not available.
        ValueError: if invalid `x` or `data_format` is passed.
    """
    if Image is None:
        raise ImportError('Could not import PIL.Image. '
                          'The use of `array_to_img` requires PIL.')
    x = np.asarray(x, dtype="float32")  # manually assigning float32 to remove keras dependency
    if x.ndim != 3:
        raise ValueError('Expected image array to have rank 3 (single image). '
                         'Got array with shape:', x.shape)

    if data_format is None:
        data_format = "channels_last"  # manually assigned to remove keras dependency
    if data_format not in {'channels_first', 'channels_last'}:
        raise ValueError('Invalid data_format:', data_format)

    # Original Numpy array x has format (height, width, channel)
    # or (channel, height, width)
    # but target PIL image has format (width, height, channel)
    if data_format == 'channels_first':
        x = x.transpose(1, 2, 0)
    if scale:
        x = x + max(-np.min(x), 0)
        x_max = np.max(x)
        if x_max != 0:
            x /= x_max
        x *= 255
    if x.shape[2] == 3:
        # RGB
        return Image.fromarray(x.astype('uint8'), 'RGB')
    elif x.shape[2] == 1:
        # grayscale
        return Image.fromarray(x[:, :, 0].astype('uint8'), 'L')
    else:
        raise ValueError('Unsupported channel number: ', x.shape[2])


def img_to_array(img, data_format=None):
    """Copied from keras with some modification to remove keras dependency.
    Converts a PIL Image instance to a Numpy array.

    # Arguments
        img: PIL Image instance.
        data_format: Image data format.

    # Returns
        A 3D Numpy array.

    # Raises
        ValueError: if invalid `img` or `data_format` is passed.
    """
    if data_format is None:
        data_format = "channels_last"  # manually assigned to remove keras dependency
    if data_format not in {'channels_first', 'channels_last'}:
        raise ValueError('Unknown data_format: ', data_format)
    # Numpy array x has format (height, width, channel)
    # or (channel, height, width)
    # but original PIL image has format (width, height, channel)
    x = np.asarray(img, dtype="float32")  # manually assigning float32 to remove keras dependency
    if len(x.shape) == 3:
        if data_format == 'channels_first':
            x = x.transpose(2, 0, 1)
    elif len(x.shape) == 2:
        if data_format == 'channels_first':
            x = x.reshape((1, x.shape[0], x.shape[1]))
        else:
            x = x.reshape((x.shape[0], x.shape[1], 1))
    else:
        raise ValueError('Unsupported image shape: ', x.shape)
    return x


def atoi(text):
    """
    helper fucntion for natural_keys
    """

    return int(text) if text.isdigit() else text
