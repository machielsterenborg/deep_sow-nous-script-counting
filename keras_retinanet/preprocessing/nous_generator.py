from collections import defaultdict
from typing import Optional, Dict

import numpy as np

from Nous.thirdparty.keras_retinanet.preprocessing.csv_generator import CSVGenerator
from Nous.thirdparty.keras_retinanet.utils.image import read_image_bgr


class NOUSCSVGenerator(CSVGenerator):
    """ Generate data for a custom CSV dataset.
    Made for NOUS, added padding around the images per discussion with Laurens and Klaas 28-09-2018

    See https://github.com/fizyr/keras-retinanet#csv-datasets for more information.
    """

    def __init__(self, pad_size, balance_images=False, image_samplings: Optional[Dict[str, int]] = None, **kwargs):
        """

        :param pad_size:
        :param balance_images: if True adapts the sampling based on image_samplings so that all images will be sampled equally often
        (statistically)
        :param image_samplings: map from (completed) image name to number of samplings, will be updated by calls to next()
        """
        self.pad_size = pad_size
        self.balance_images = balance_images
        self.image_samplings = image_samplings
        if self.image_samplings is None:
            self.image_samplings = defaultdict(lambda: 0)

        super(NOUSCSVGenerator, self).__init__(**kwargs)

    def load_image(self, image_index):
        """ Load an image at the image_index.
        """
        image_path = self.image_path(image_index)

        image = read_image_bgr(image_path)
        if self.pad_size > 0:
            new_im = np.zeros(shape=(image.shape[0] + (self.pad_size * 2), image.shape[1] + (self.pad_size * 2), image.shape[2]))
            new_im[self.pad_size:self.pad_size + image.shape[0], self.pad_size:self.pad_size + image.shape[1], :] = image
            image = new_im

        self.image_samplings[image_path] += 1
        return image

    def load_annotations(self, image_index):
        """ Load annotations for an image_index.
        """
        path = self.image_names[image_index]
        annotations = self.image_data[path]
        boxes = np.zeros((len(annotations), 5))

        for idx, annotation in enumerate(annotations):
            class_name = annotation['class']
            boxes[idx, 0] = float(annotation['x1']) + self.pad_size
            boxes[idx, 1] = float(annotation['y1']) + self.pad_size
            boxes[idx, 2] = float(annotation['x2']) + self.pad_size
            boxes[idx, 3] = float(annotation['y2']) + self.pad_size
            boxes[idx, 4] = self.name_to_label(class_name)

        return boxes

    def image_imbalance(self) -> float:
        """
        Returns an imbalance measure ranging from 0.0 (no imbalance) to 1.0 (complete imbalance)
        :return: float in [0,1]
        """
        for image_index in range(len(self.image_names)):
            if self.image_path(image_index) not in self.image_samplings:
                self.image_samplings[self.image_path(image_index)] = 0

        samplings = np.array(list(self.image_samplings.values()))
        return 1.0 if len(samplings) == 0 else (samplings.max() - samplings.min()) / samplings.sum()

    def next(self):
        """Yields a batch
        """
        if not self.balance_images:
            result = super(NOUSCSVGenerator, self).next()
        else:
            with self.lock:
                num_images = len(self.image_names)
                image_range = range(num_images)
                samplings = np.array([self.image_samplings[self.image_path(image_index)] for image_index in image_range], dtype=float)
                if samplings.sum() > 0 and (samplings.max() - samplings).sum() > 0:
                    sample_probability = samplings.max() - samplings
                else:
                    sample_probability = np.ones_like(samplings)
                sample_probability /= sample_probability.sum()
                # print(samplings, sample_probability)
                group = np.random.choice(list(image_range), size=self.batch_size, replace=True, p=sample_probability)
                result = self.compute_input_output(group)

        return result
