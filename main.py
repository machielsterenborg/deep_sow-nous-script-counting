import glob
import os

import cv2
import numpy as np

import utils  # custom utils script with all necessary functions
from keras_retinanet.models import load_model

model = load_model("inference_model.h5")

### USER CONFIGURABLE PARAMS ###
min_confidence = 0.35  # Minimum confidence for the box to be drawn (0.0 -> 1.0)
video_mode = True  # Whether to produce a video, or alternatively simply save the processed images in the output folder
video_framerate = 5

humansort = False

def main():
    image_filenames = glob.glob('dataset/*.tif', recursive=True)
    os.makedirs("output", exist_ok=True)
    frame_list = []

    if humansort:
        image_filenames.sort(key=utils.natural_keys)

    # Load the images and format them
    uids, images, scales, heights, widths, size_before_padding, target_side = utils.load_data_for_detection_inference(
        image_filenames, 2, 600, 1200)

    # Use the trained model to get bounding boxes, their confidence scores, and the associated labels
    predictions = model.predict(np.vstack(images))
    predicted_boxes, predicted_scores, predicted_labels = predictions[0:3]

    # Loop through all of our images
    for i, image_filename in enumerate(image_filenames):
        uid = os.path.splitext(os.path.basename(image_filename))[0]
        predicted_boxes[i] = utils.compensate_squaring_on_bounding_boxes(predicted_boxes[i], size_before_padding[i],
                                                                         target_side)
        predicted_boxes[i] /= scales[i]

        # Filter the found bounding boxes to remove overlapping boxes, and boxes with low confidence
        pick = utils.non_max_suppression(predicted_boxes[i], predicted_scores[i], predicted_labels[i],
                                         nms_threshold=0.4,
                                         score_threshold=min_confidence,
                                         num_classes=utils.get_num_classes())
        filtered_boxes = predicted_boxes[i][pick]
        filtered_scores = predicted_scores[i][pick]
        filtered_predicted_labels = predicted_labels[i][pick]

        class_id_to_name = utils.get_class_id_to_name("labels.txt")
        buffer_table = utils.create_buffer_table_from_detection_results(uid, filtered_boxes, filtered_predicted_labels,
                                                                        filtered_scores,
                                                                        min_confidence, 2, widths[i], heights[i],
                                                                        class_id_to_name)

        processed_prediction = []
        found_boxes = 0
        for _uid, label_idx, label, score, x1, y1, x2, y2, in buffer_table:
            found_boxes += 1
            processed_prediction.append([[x1, y1, x2, y2], label_idx, label])

        # Add bounding boxes and box count to frame, save them to a list
        frame_list.append(utils.draw_boxes_and_count_on_image(image_filename, processed_prediction, found_boxes))

    # Write the list of frames to a video file, or alternatively a set of images
    if video_mode:
        height, width, layers = np.array(frame_list[0]).shape
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        video = cv2.VideoWriter('output/output.avi', fourcc, video_framerate, (width, height), True)

        for frame in frame_list:
            video.write(np.array(frame))

        cv2.destroyAllWindows()
        video.release()
    else:
        for j, frame in enumerate(frame_list):
            frame.save(f"output/img{j}.jpg")


if __name__ == "__main__":
    main()
